local lg = love.graphics        -- Чтобы не писать везде love.graphics сделаем сокращение до lg

-- Константы
local MAX_X = 854               -- Ширина окна
local MAX_Y = 480               -- Высота окна

local WORK_H        = 250       -- Высота рабочей области
local Y0            = 300       -- Ноль по вертикали (там, где горизонт)
local TOP           = Y0 - WORK_H
local EARTH_Y       = Y0 - 12   -- Координата земли
local DINO_X        = 80        -- Расстояние от края до динозавра
local TEXT_POS      = 30

local GRAVITY       = 1700      -- Гравитация
local STEP_T        = 0.2       -- Период шагов (в секундах)
local JUMP_PULSE    = 500       -- Начальная скорость прыжка
local DINO_LIFES    = 5
local MIN_SPEED     = 125       -- Минимальная скорость динозавра (пикселей в секунду)
local MAX_SPEED     = 600       -- Максимальная скорость динозавра (пикселей в секунду)

local SPEED_PER_SECOND = 1      -- Приращение скорости за секунду
local SCORE_PER_SECOND = 10     -- Приращение очков за секунду

local CACTUS_COUNT  = 11        -- Количество видов (спрайтов) кактуса (0..10)
local CACTUS_SMALL  = 5         -- Маленькие кактусы от 0 до 5
local CACTUS_LARGE  = 9         -- Большие кактусы от 6 до 9
local CACTUS_HUGE   = 10        -- Очень большой кактус 10

-- Вычисляемые константы
local SCALE

-- При изменении размера окна меняется масштаб
function love.resize(w, h)
   SCALE = w/MAX_X
end

-- Вызывается один раз при загрузке игры
function love.load()
   -- Выставляем размер окна
   love.window.setMode(MAX_X, MAX_Y, {resizable = true})

   -- Убираем сглаживание при масштабировании
   lg.setDefaultFilter('linear', 'nearest')

   -- Белый фон
   lg.setBackgroundColor(1, 1, 1);

   -- Вычисляем масштаб
   local ww, _ = lg.getDimensions()
   SCALE = ww/MAX_X

   -- Загружаем карту спрайтов
   tiles = lg.newImage('images/tiles.png')

   -- Здесь будут храниться координаты спрайтов на карте
   dino_sprite = {}
   -- 0 - стоит
   -- 1 - стоит с закрытыми глазами
   -- 2 - левая нога поднята
   -- 3 - правая нога поднята
   -- 4 - мертвый динозавр

   local DINO_W = 44            -- Ширина спрайта с динозавром
   local DINO_H = 47            -- Высота спрайта с динозавром

   -- Заполняем массив с координатами спрайтов динозавра
   for i = 0, 4 do
      dino_sprite[i] = lg.newQuad(848+i*DINO_W, 2, DINO_W, DINO_H, tiles:getDimensions())
   end

   -- Летящий динозавр
   -- 5 - крылья вверх
   -- 6 - крылья вниз
   for i = 0, 1 do
      dino_sprite[i+5] = lg.newQuad(i*64, 75, 64, 49, tiles:getDimensions())
   end

   -- Спрайт земли (начало (2:54) размер 1200х12)
   earth_sprite = lg.newQuad(2, 54, 1200, 12, tiles:getDimensions())

   -- Спрайты кактусов
   cactus_sprite = {}

   local S_CACT_W = 17
   local S_CACT_H = 35

   -- Маленькие кактусы
   for i = 0, 5 do
      cactus_sprite[i] = lg.newQuad(228+i*S_CACT_W, 2, S_CACT_W, S_CACT_H, tiles:getDimensions())
   end

   local L_CACT_W = 25
   local L_CACT_H = 50

   -- Большие кактусы (от 6 до 9)
   for i = 0, 4 do
      cactus_sprite[i+6] = lg.newQuad(332+i*L_CACT_W, 2, L_CACT_W, L_CACT_H, tiles:getDimensions())
   end

   -- Самый большой кактус (10)
   cactus_sprite[10] = lg.newQuad(431, 2, 51, L_CACT_H, tiles:getDimensions())

   -- Спрайт облака
   cloud_sprite = lg.newQuad(86, 2, 47, 14, tiles:getDimensions())

   -- Числа
   number_sprite = {}

   for i = 0, 9 do
      number_sprite[i] = lg.newQuad(655+i*10, 2, 10, 11, tiles:getDimensions())
   end

   -- Спрайт жизни
   life_img = {}
   life_img.life = lg.newImage('images/life.png')
   life_img.dead = lg.newImage('images/dead.png')

   -- Инициализируем генератор случайных чисел
   math.randomseed(os.time())
end

------------------------------------------------------
-- Игровые ништяки
------------------------------------------------------
local WINGS_TIME        = 20
local WINGS_FLY_TIME    = 3

local stuff = {
   wings = { en = false, timer = 0, canfly = false, fly_timer = 0 }
}

------------------------------------------------------
-- Обновление состояния динозавра
------------------------------------------------------
-- Переменные состояния динозавра
local dino_y     = 0            -- Координата Y
local dino_frame = 0            -- Номер текущего спрайта
local dino_state = 1            -- Номер состояния
local dino_predead              -- Состояние перед столкновением
local step_timer = 0            -- Таймер шагов (смена спрайта при шагании)
local dino_lifes = DINO_LIFES   -- Жизни

-- Скорость по оси X (пикселей в секунду)
-- Минимальная скорость 125 - можно перепрыгнуть только маленькие кактусы
-- На скорости 150 можно перепрыгнуть большие, но очень большие нельзя
-- На скорости 200 и выше можно перепрыгивать все кактусы
local speed_x    = MIN_SPEED
local speed_y                   -- Текущая скорость динозавра по оси Y

-- Таймер режима Бога (используется при продолженнии после потери жизни)
local dino_god   = false
local dino_god_timer = 0
local DINO_GOD_TIME = 3

-- Таймер мигания динозавра в режиме Бога
local dino_hide = false
local dino_blink_timer = 0
local DINO_BLINK_T = 0.25

-- Возможные состояния динозавра
local DINO_STAND    = 0            -- Стоит
local DINO_RUN      = 1            -- Бежит
local DINO_JUMP     = 2            -- Прыгает
local DINO_FLY      = 3            -- Летит
local DINO_DEAD     = -1           -- Умер

function dino_step(dt)
   local step

   -- Если таймер шагов меньше половины периода шага...
   if (step_timer < (STEP_T/2)) then
      -- первый шаг
      step = 0;
   else
      -- второй шаг
      step = 1;
   end

   -- Крутим таймер шагов
   step_timer = step_timer + dt;
   if (step_timer > STEP_T) then
      step_timer = step_timer - STEP_T;
   end

   return step
end

function update_dino(dt)
   -- Конечный автомат для вычисления состояния динозавра
   dino_fsm = {
      -- Просто стоим
      [DINO_STAND] = function()
         dino_y = 0;
         dino_frame = 0;

         return DINO_STAND
      end,

      -- Бежим и перебираем ножками
      [DINO_RUN] = function()
         local state = DINO_RUN
         dino_y = 0;

         -- Если шаг=0, то frame=2. Если шаг=1, то frame=3
         dino_frame = 2 + dino_step(dt)

         -- Если нажат пробел, запускаем прыжок
         if love.keyboard.isDown("space") or
            love.mouse.isDown(1)
         then
            if stuff.wings.canfly then
               state = DINO_FLY
            else
               state = DINO_JUMP
            end

            speed_y = JUMP_PULSE
         end

         return state
      end,

      -- В процессе прыжка
      [DINO_JUMP] = function()
         local state = DINO_JUMP

         -- Прибавляем координату Y в соответствии со скоростью
         dino_y = dino_y + (speed_y * dt)

         -- Если координата менше нуля (уже летим вниз и долетели до земли)...
         if (dino_y <= 0) then
            -- то возвращаемся к бегу
            dino_y = 0
            state = DINO_RUN
         else
            -- иначе продолжаем лететь, а скорость уменьшаем на значение гравитации
            speed_y = speed_y - (GRAVITY * dt);
         end

         -- Летим в позе "стоя"
         dino_frame = 0

         return state
      end,

      -- Динозавр летит (почти то-же самое, что прыжок, только машем крыльями)
      [DINO_FLY] = function()
         local state = DINO_FLY

         -- Прибавляем координату Y в соответствии со скоростью
         dino_y = dino_y + (speed_y * dt)

         -- Если координата менше нуля (уже летим вниз и долетели до земли)...
         if (dino_y <= 0) then
            -- то возвращаемся к бегу
            dino_y = 0
            state = DINO_RUN
         else
            -- иначе продолжаем лететь, а скорость уменьшаем на значение гравитации
            speed_y = speed_y - (GRAVITY * dt);
         end

         if (dino_y > WORK_H) then
            speed_y = 0
            dino_y = WORK_H
         end

         -- Если шаг=0, то frame=5. Если шаг=1, то frame=6
         dino_frame = 5 + dino_step(dt)

         -- Если нажат пробел, снова прыгаем
         if love.keyboard.isDown("space") or
            love.mouse.isDown(1)
         then
            state = DINO_FLY
            speed_y = JUMP_PULSE
         end

         -- Проверяем, можем ли летать. Если нет, то продолжаем как прыжок
         if not stuff.wings.canfly then
            state = DINO_JUMP
         end

         return state
      end,

      -- Конец игры
      [DINO_DEAD] = function()
         dino_frame = 4

         -- Если нажат пробел, включаем God-mode и продолжаем игру
         if love.keyboard.isDown("escape") or
            love.mouse.isDown(1)
         then
            dino_god = true
            dino_god_timer = DINO_GOD_TIME
            return dino_predead
         else
            return DINO_DEAD
         end
      end,
   }

   -- Следующее состояние
   dino_state = dino_fsm[dino_state]()

   -- Таймер режима Бога
   if dino_god_timer > 0 then
      dino_god_timer = dino_god_timer - dt
   else
      dino_god = false;
      dino_hide = false;
   end

   -- Мигание динозавра в режиме Бога
   if dino_god then
      if dino_blink_timer <= 0 then
         dino_blink_timer = DINO_BLINK_T
      else
         dino_blink_timer = dino_blink_timer - dt
      end

      dino_hide = dino_blink_timer < (DINO_BLINK_T / 2)
   end

   -- В процессе игры постепенно увеличиваем скорость
   if dino_state == DINO_JUMP or
      dino_state == DINO_RUN or
      dino_state == DINO_FLY
   then
      if speed_x < MAX_SPEED then
         speed_x = speed_x + (SPEED_PER_SECOND * dt)
      end
   end


   -- ----- TEMPORARY --------------
   if love.keyboard.isDown('f')
   then
      stuff.wings.en = true
      stuff.wings.timer = WINGS_TIME
      stuff.wings.canfly = true
      stuff.wings.fly_timer = WINGS_FLY_TIME
   end


end

------------------------------------------------------
-- Функция обновления земли
------------------------------------------------------
-- Переменные состояния земли
local earth_x    = 0            -- Координата земли относительно динозавра

function update_earth(dt)
   -- Если динозавр бежит или прыгает, двигаем землю
   if dino_state == DINO_JUMP or
      dino_state == DINO_RUN or
      dino_state == DINO_FLY
   then
      -- Достаём длину спрайта земли
      local _, _, w, _ = earth_sprite:getViewport()

      -- Двигаем землю, и если координата дальше длины спрайта, возвращаем в 0
      earth_x = earth_x - (speed_x * dt)
      if (earth_x < -w) then
         earth_x = earth_x + w
      end
   end
end

------------------------------------------------------
-- Функция обновления кактусов
------------------------------------------------------
-- Кактусы
local MAX_CACTUSES      = 5
local MIN_CACT_DIST_K   = 0.5   -- Минимальное расстояние между кактусами
                                -- как коэффициент от скорости
local MAX_CACT_DIST_K   = 2

local COMPLEX_1_SPEED   = 300   -- Скорость, при которой слоэность равна 1
local COMPLEX_K         = 0.6   -- Коэффициент сложности (0..1). Чем меньше - тем проще в начале и сложнее в конце

local cactuses          = {}
local cactus_count      = 0
local next_cactus_x     = 0

function update_cactuses(dt)
   -- Если динозавр бежит или прыгает, двигаем кактусы
   if dino_state == DINO_JUMP or
      dino_state == DINO_RUN or
      dino_state == DINO_FLY
   then
      -- Двигаем кактусы
      for _, c in pairs(cactuses) do
         c.x = c.x - (speed_x * dt)

         -- Убираем кактусы, которые ушли за границу экрана
         if c.x < -50 then
            cactuses[c] = nil
            cactus_count = cactus_count - 1
         end
      end

      -- Проверим, достаточно ли расстояние до нового кактуса
      if next_cactus_x <= 0 then
         -- Можно добавлять кактус, но сначала проверим на максимальное кол-во кактусов
         if cactus_count < MAX_CACTUSES then
            -- Выбираем кактус исходя из текущей скорости
            -- (на медленной скорости невозможно перепрыгнуть большой кактус)
            local max_cactus
            if speed_x > 300 then
               max_cactus = CACTUS_HUGE
            elseif speed_x > 200 then
               max_cactus = CACTUS_LARGE
            else
               max_cactus = CACTUS_SMALL
            end

            -- Создаем новый кактус
            local cact = {
               x = MAX_X,
               sprite = cactus_sprite[math.random(0, max_cactus)]
            }
            cactuses[cact] = cact

            -- Задаем случайное расстояние до следующего кактуса с учетем
            -- минимального и максимального расстояния, ширины вновь созданного
            -- кактуса, скорости и сложности
            local _, _, cw, _ = cact.sprite:getViewport()
            -- local dist_koeff = speed_x * COMPLEX_K + COMPLEX_1_SPEED * (1 - COMPLEX_K)
            local dist_koeff = COMPLEX_K * (speed_x - COMPLEX_1_SPEED) + COMPLEX_1_SPEED
            next_cactus_x = math.random(dist_koeff * MIN_CACT_DIST_K,
                                        dist_koeff * MAX_CACT_DIST_K) + cw

            -- Увеличиваем счетчик кактусов
            cactus_count = cactus_count + 1
         end
      else
         -- Приближаем новый кактус
         next_cactus_x = next_cactus_x - (speed_x * dt)
      end
   end
end

------------------------------------------------------
-- Контроль пересечения динозавра с препятствиями
------------------------------------------------------
local INTERSECT = 0.25

function square_intersect(ax, ay, aw, ah, bx, by, bw, bh)
   local ax1 = ax + aw
   local ay1 = ay + ah
   local bx1 = bx + bw
   local by1 = by + bh

   -- Проверяем на непересечение
   return not ((ax > bx1) or (bx > ax1) or (ay > by1) or (by > ay1))
end

function dino_intersection()
   -- Если активна God-mode, то ничего не проверяем
   if dino_god then
      return
   end

   -- Получаем размеры динозавра
   local _, _, dw, dh = dino_sprite[dino_frame]:getViewport()

   -- Пересчитываем с учетом наложения
   dw = dw * (1-INTERSECT)
   dh = dh * (1-INTERSECT)

   -- Проверяем пересечение с кактусами
   local intersection = false

   if dino_state == DINO_RUN or
      dino_state == DINO_JUMP or
      dino_state == DINO_FLY
   then
      for _, c in pairs(cactuses) do
         local _, _, cw, ch = c.sprite:getViewport()
         cw = cw * (1-INTERSECT)
         ch = ch * (1-INTERSECT)

         intersection = square_intersect(DINO_X, dino_y, dw, dh, c.x, 0, cw, ch)
         if intersection then break end
      end
   end

   if intersection then
      dino_lifes = dino_lifes - 1;
      if (dino_lifes < 0) then
         dino_lifes = 0;
      end

      dino_predead = dino_state
      dino_state = DINO_DEAD
   end
end

------------------------------------------------------
-- Обновление облаков
------------------------------------------------------
local CLOUD_N = 5               -- Количество облаков
local cloud = {}                -- Массив облаков

-- Функция для создания нового облака
function make_cloud()
   return { x = math.random(MAX_X, MAX_X*3),
            y = math.random(Y0-150, Y0-50) }
end

function update_clouds(dt)
   if dino_state == DINO_RUN or
      dino_state == DINO_JUMP or
      dino_state == DINO_FLY
   then
      -- Перебираем массов облаков
      for i = 1, CLOUD_N do
         -- Если облака еще нет, создадим
         if cloud[i] == nil then
            cloud[i] = make_cloud()
         else
            -- Двигаем облако
            cloud[i].x = cloud[i].x - (speed_x * 0.3 * dt)

            -- Если облако ушло за экран, заменяем его новым облаком
            if cloud[i].x < -100 then
               cloud[i] = make_cloud()
            end
         end
      end
   end
end

------------------------------------------------------
-- Игровой счёт
------------------------------------------------------
local SCORE_LEN = 6
local score = 0

function update_score(dt)
   if dino_state == DINO_RUN or
      dino_state == DINO_JUMP or
      dino_state == DINO_FLY
   then
      score = score + (SCORE_PER_SECOND * dt)
   end
end


function update_stuff(dt)
   ------ Wings -----
   if dino_state == DINO_RUN or
      dino_state == DINO_JUMP or
      dino_state == DINO_FLY
   then
      if stuff.wings.en then
         if stuff.wings.timer > 0 then
            stuff.wings.timer = stuff.wings.timer - dt
            stuff.wings.canfly = true

            if dino_state == DINO_RUN then
               stuff.wings.fly_timer = stuff.wings.fly_timer + dt
               if stuff.wings.fly_timer > WINGS_FLY_TIME then
                  stuff.wings.fly_timer = WINGS_FLY_TIME
               end
            end

            if dino_state == DINO_FLY then
               stuff.wings.fly_timer = stuff.wings.fly_timer - dt
               if stuff.wings.fly_timer < 0 then
                  stuff.wings.fly_timer = 0
                  stuff.wings.canfly = false
               end
            end
         else
            stuff.wings.en = false
            stuff.wings.canfly = false
         end
      end
   end
end



------------------------------------------------------
-- Обновление сцены
------------------------------------------------------
function love.update(dt)
   -- Обновляем
   update_dino(dt)
   update_earth(dt)
   update_cactuses(dt)
   update_clouds(dt)
   update_score(dt)

   update_stuff(dt)

   dino_intersection()
end

------------------------------------------------------
-- Перерисовка экрана
------------------------------------------------------
function draw_numbers(num, len, x, y)
   local dig = 10 ^ (len-1)
   num = math.floor(num)

   for i = 1, len do
      local n = 0

      if num >= dig then
         n = math.floor(num/dig)
         num = num % dig
      end

      dig = dig / 10

      local _, _, w, _ = number_sprite[n]:getViewport()
      lg.draw(tiles, number_sprite[n], x, y)
      x = x + w
   end
end

function draw_lifes(lifes, max, x, y)
   local w = life_img.life:getWidth()
   for i = 0, lifes-1 do
      lg.draw(life_img.life, x, y)
      x = x + w
   end

   local w = life_img.dead:getWidth()
   for i = lifes, max-1 do
      lg.draw(life_img.dead, x, y)
      x = x + w
   end
end

function draw_stuff()
   -- WINGS
   if stuff.wings.en then
      if stuff.wings.timer > 2 or
         (stuff.wings.timer*4 - math.floor(stuff.wings.timer*4)) < 0.5
      then
         local r, g, b, a = lg.getColor()
         lg.setColor(0.32, 0.32, 0.32)
         lg.rectangle('line', 200, TEXT_POS, 150, 10)
         lg.rectangle('fill', 200, TEXT_POS, 150 * stuff.wings.fly_timer / WINGS_FLY_TIME, 10)
         lg.setColor(r, g, b, a)
      end
   end
end

function love.draw()
   lg.push()
   -- lg.translate(0 , transAmount )
   lg.scale(SCALE, SCALE)


   -- Рисуем землю
   local _, _, ew, _ = earth_sprite:getViewport()
   for ex = earth_x, MAX_X, ew do
      lg.draw(tiles, earth_sprite, math.floor(ex), EARTH_Y)
   end

   -- Рисуем облака
   for i = 1, CLOUD_N do
      local c = cloud[i]
      if not(c == nil) then
         lg.draw(tiles, cloud_sprite, math.floor(c.x), math.floor(c.y), 0, 1, 1, 0, dh)
      end
   end

   -- Счёт и жизни
   draw_numbers(score, SCORE_LEN, MAX_X - 150, TEXT_POS)
   draw_numbers(speed_x, 3, MAX_X - 250, TEXT_POS)
   draw_lifes(dino_lifes, DINO_LIFES, 50, TEXT_POS)

   -- Ништяки
   draw_stuff()

   -- Рисуем кактусы
   for _, c in pairs(cactuses) do
      local _, _, _, ch = c.sprite:getViewport()
      lg.draw(tiles, c.sprite, math.floor(c.x), Y0-ch)
   end

   -- Рисуем динозавра
   if not dino_hide then
      local _, _, _, dh = dino_sprite[dino_frame]:getViewport()
      lg.draw(tiles, dino_sprite[dino_frame], DINO_X, math.floor(Y0 - dino_y), 0, 1, 1, 0, dh)
   end

   lg.pop()
end
